using System.Linq;
using System.Reflection.Metadata.Ecma335;

namespace Utils
{
    public class StringUtils
    {
        public static string SimplifyBinaryString(string b)
        {
            var charArray = b.ToCharArray();
            var leadingChars = charArray.TakeWhile(c => c == charArray[0]).ToArray();
            
            if (leadingChars.Length == charArray.Length)
            {
                return new string(charArray[0], 1);
            }

            return b.Substring(leadingChars.Length - 1);
        }
    }
}