﻿namespace Chapter0

module Fibonacci =
    
    let rec recursiveFib n =
        match n with
        | 1 -> 0
        | 2 -> 1
        | x -> recursiveFib ( x - 1 ) + recursiveFib (x - 2)
        
    let dynamicFib n =
        let nMinusOne = n - 1
        let memory = Array.append [| 0; 1 |] (Array.zeroCreate (n - 1))
        for i in 2 .. nMinusOne do
            Array.set memory i (memory.[i - 1] + memory.[i - 2])
        memory.[nMinusOne]
        
    let rec idiomaticFibImpl n a b =
        if n > 0 then
            let next = a + b
            let rest = idiomaticFibImpl (n - 1) b next 
            next::rest
        else 
            []
     
    let idomaticFib n =
        let a = 0
        let b = 1
        
        let lst = a::b::(idiomaticFibImpl (n - 2) a b)
        List.last lst
        
    let unfoldFib n =
        let fib = seq {
            yield 0
            yield 1
            
            yield! Seq.unfold (fun (n, a, b) ->
                if n = 0 then None
                else Some(a+b, (n-1, b, a+b))) (n-2, 0, 1)
        }
        
        Seq.last fib
