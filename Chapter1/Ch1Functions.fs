﻿namespace Chapter1

module BasicArithmetic =
    open Utils
    open System
    
    let isOdd (x:string) = x.Chars(x.Length - 1) = '1'
    let isEven x = not (isOdd x)
    let isZero (x:string) = not (x.Contains("1"))
    let positiveOne = "01"
    let floorDiv2 (x:string) = x.Substring(0, x.Length - 1)
    
    // addition is linear in n because every pair of bits must be independently summed, for a total of n-1 summations
    let add (n:string) (m:string) =
        
        let sumCarry (a, b) c =
            let lst = a::b::c::[]
            let count = Seq.length (List.filter (fun x -> x = '1') lst)
            match count with
            | 0 -> '0', '0'
            | 1 -> '1', '0'
            | 2 -> '0', '1'
            | _ -> '1', '1'
        
        // Make sure the two numbers are of the same length
        let nMSD = if n.Length > 0 then n.Chars(0) else '0'
        let mMSD = if m.Length > 0 then m.Chars(0) else '0'
        
        let maxLen = max n.Length m.Length
        let paddedN = new string(nMSD, maxLen - n.Length + 1) + n // always pad at least one char
        let paddedM = new string(mMSD, maxLen - m.Length + 1) + m // always pad at least one char
        let zipped = Seq.zip paddedN paddedM
        
        let result =
            fst (Seq.mapFoldBack sumCarry zipped '0')
            |> Array.ofSeq
            |> String
            
           
        Utils.StringUtils.SimplifyBinaryString result
            
    
    let private negate (n:string) =
        let inverted = n.ToCharArray()
                       |> Array.map (fun x -> if x = '0' then '1' else '0')
                       |> (fun x -> new String(x))
                       
        add inverted positiveOne
        
    let subtract n m =
        add n (negate m)
        
    let private geq (x:string) (y:string) = (subtract x y).Chars(0) = '0'
        
    let multiply (n:string) (m:string) =
        let indices = [0 .. (m.Length - 1)]
        
        let foldFun accumulator idx =
            let leftShifted = n + new string('0', idx)
            if m.Chars(m.Length - idx - 1) = '1' then
                add accumulator leftShifted
            else accumulator
            
        let result = List.fold foldFun "" indices
        result
        
 
    let rec frenchMultiplication (n:string) (m:string) =
        if isZero m then
            m
        else
            let z = frenchMultiplication n (m.Substring(0, m.Length - 1)) // divide Z by two, then multiply
            if isEven m then // m is even
                z + "0" // multiply z by 2
            else
                add n (z + "0") //multiply z by 2
                
 
    /// Divides n-bit numbers x by y, where y \geq 1
    /// Returns the quotient and remainder of x divided by y
    let rec divide (x:string) (y:string) =
        if isZero x then
            ("0", "0")
        else
            let mutable q, r = divide (x.Substring(0, x.Length - 1)) y
            q <- q + "0"
            r <- r + "0"
            r <- if (isOdd x) then add r positiveOne else r
            let a, b = if geq r y then (add q positiveOne), (subtract r y) else q, r
            q <- a
            r <- b
            q, r
 
module ModularArithmetic =
    open BasicArithmetic
    
    let rec modExp x y n =
        if isZero y then
            positiveOne
        else
            let yFloor = floorDiv2 y
            let z = modExp x yFloor n
            let zSquaredModN = snd (divide (multiply z z) n)
            if (isEven y) then
                zSquaredModN
            else
                multiply x zSquaredModN
 
 
    let euclidGcd n m =
        0
 
 
    let extendedEuclidGcd n m =
        0
 
