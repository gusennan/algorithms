module Tests

open Chapter1
open System.Numerics
open Xunit
open BasicArithmetic
open ModularArithmetic
open Utils

let toBin (x:int) = (new BigInteger(x)).ToBinaryString()
let fromBin (x:string) = BigIntegerExtensions.FromBinaryString(x)

[<Fact>]
let addTest () =
    let oneMillion = "011110100001001000000"
    let sixShift = oneMillion + new string('0', 6)
    let nineShift = oneMillion + new string('0', 9)
    let sum = add sixShift nineShift
    Assert.Equal("0100010010101010001000000000000", sum)

[<Fact>]
let multiplyTest () =
    let one = "01"
    let three = "011"
    let shouldBeThree = multiply one three
    Assert.Equal(three, shouldBeThree)
    
    let oneMillion = toBin 1000000
    let oneTrillion = (new BigInteger(1_000_000) * new BigInteger(1_000_000)).ToBinaryString()
    let shouldBeOneTrillion = multiply oneMillion oneMillion
    Assert.Equal(oneTrillion, shouldBeOneTrillion)
    
    let oneMillion = toBin 1000000
    let negativeOneMillion = toBin -1000000
    let negativeOneTrillion = (new BigInteger(-1_000_000) * new BigInteger(1_000_000)).ToBinaryString()
    let shouldBeNegativeOneTrillion = multiply oneMillion negativeOneMillion
    Assert.Equal(oneTrillion, shouldBeOneTrillion)
    
    
[<Fact>]
let ``modExpTest`` () =
    let threeFiveFour = fromBin (modExp (toBin 3) (toBin 5) (toBin 4))
    Assert.Equal(threeFiveFour, new BigInteger(3))
    
    let millionBillionTwenty = fromBin (modExp (toBin 1000000) (toBin 1000000000) (toBin 20))
    Assert.Equal(new BigInteger(0), millionBillionTwenty)
