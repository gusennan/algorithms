﻿// Learn more about F# at http://fsharp.org

open Chapter1
open System.Numerics
open Utils
open Utils
    
//let private toBin (x:int) = BigInteger Convert.ToString(x, 2).PadLeft(32, '0')
//let private FromBin (x:int) = Convert(x, 2).PadLeft(32, '0')

[<EntryPoint>]
let main argv =
    let n = new BigInteger(20)
    let bs = n.ToBinaryString()
    let m = new BigInteger(20)
    let bs2 = m.ToBinaryString()
    let o = new BigInteger(2000000)
    let os = o.ToBinaryString()
    let p = new BigInteger(2000000)
    let ps = p.ToBinaryString()
    
    let toBin (x:int) = (new BigInteger(x)).ToBinaryString()
    let fromBin (x:string) = BigIntegerExtensions.FromBinaryString(x).ToString()
    
    (*
    printfn "Hello World from F#!"
    printfn "recursiveFib on %d = %d" n (Fibonacci.recursiveFib n)
    printfn "dynamicFib on %d = %d" n (Fibonacci.dynamicFib n)
    printfn "idiomaticFib on %d = %d" n (Fibonacci.idomaticFib n)
    printfn "unfoldFib on %d = %d" n (Fibonacci.unfoldFib n)
  
    printfn "adding %d + %d = %d" 3 2 (BasicArithmetic.add (toBin 3) (toBin 2))
    printfn "adding %d + %d = %d" 3 -2 (BasicArithmetic.add (toBin 3) (toBin -2))
    printfn "adding %d + %d = %d" -3 -2 (BasicArithmetic.add (toBin -3) (toBin -2))
    *)
    
    printfn "subtract %d - %d = %s" 3 2 (fromBin (BasicArithmetic.subtract (toBin 3) (toBin 2)))
    printfn "subtract %d - %d = %s" 3 -2 (fromBin (BasicArithmetic.subtract (toBin 3) (toBin -2)))
    printfn "subtract %d - %d = %s" -3 -2 (fromBin (BasicArithmetic.subtract (toBin -3) (toBin -2)))
    
    (*
    printfn "multiply %s by %s = %s" (n.ToString()) (m.ToString()) ((BigIntegerExtensions.FromBinaryString(BasicArithmetic.multiply bs bs2)).ToString())
    printfn "frenchMultiply %s by %s = %s" (n.ToString()) (m.ToString()) ((BigIntegerExtensions.FromBinaryString(BasicArithmetic.frenchMultiplication bs bs2)).ToString())
    printfn "multiply %s by %s = %s" (o.ToString()) (p.ToString()) ((BigIntegerExtensions.FromBinaryString(BasicArithmetic.multiply os ps)).ToString())
    printfn "frenchMultiply %s by %s = %s" (o.ToString()) (p.ToString()) ((BigIntegerExtensions.FromBinaryString(BasicArithmetic.frenchMultiplication os ps)).ToString())
    *)
    
    let twenty = new BigInteger(20)
    let three = new BigInteger(21)
    let result = BasicArithmetic.subtract (twenty.ToBinaryString()) (three.ToBinaryString())
    printfn "subtract %s - %s = %s" (twenty.ToString()) (three.ToString()) (BigIntegerExtensions.FromBinaryString(result).ToString())
    
    (*
    let quotient, remainder = BasicArithmetic.divide (twenty.ToBinaryString()) (three.ToBinaryString())
    printfn "divide %s by %s = %s, %s" (twenty.ToString()) (three.ToString()) (BigIntegerExtensions.FromBinaryString(quotient).ToString()) (BigIntegerExtensions.FromBinaryString(remainder).ToString())
    
    let fourHundred = new BigInteger(400)
    let eighteen = new BigInteger(18)
    let quotient, remainder = BasicArithmetic.divide (fourHundred.ToBinaryString()) (eighteen.ToBinaryString())
    printfn "divide %s by %s = %s, %s" (fourHundred.ToString()) (eighteen.ToString()) (BigIntegerExtensions.FromBinaryString(quotient).ToString()) (BigIntegerExtensions.FromBinaryString(remainder).ToString())
    *)
    
    0 // return an integer exit code
